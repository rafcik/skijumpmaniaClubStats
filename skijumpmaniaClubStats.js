// ==UserScript==
// @name         skijumpmaniaClubStats
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Pozwala zbierać statystyki graczy (ilosć wygranych mk, prestiż) w klubie.
// @author       rafcik
// @match        http://s1.skijumpmania.com/*
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_log
// @require      http://code.jquery.com/jquery-2.1.4.min.js
// ==/UserScript==
/* jshint -W097 */
'use strict';

var game = new Game();

function createElement(type, attributes, append, inner) {
    try {
        var node = document.createElement(type);
        for (var attr in attributes) {
            if (!attributes.hasOwnProperty(attr)){ 
                continue; 
            }
            
            if (attr=="checked") { 
                node.checked=attributes[attr]; 
            } else { 
                node.setAttribute(attr, attributes[attr]); 
            }
        }
        if (append) { 
            append.appendChild(node);
        }
        if (inner) {
            node.innerHTML = inner;
        }
        
        return node;
    } catch(err) { 
        GM_log("createElement","type="+type+" attributes="+JSON.stringify(attributes)+" append="+append+" append.id="+(append&&append.id?append.id:"?"),"",err); 
    }
}

function getSpaces(count) {
    var spaces = "";
    
    for(var i=0; i<count; i++) {
        spaces += '_';
    }
    
    return spaces;
}

function Game() {
    this.PLAYERS_CLUB_STATS = 'PLAYERS_CLUB_STATS';
    
    this.render = function() {
        if(window.location.pathname == "/news/clubs/" && $('.profile_club_list_players_header')) {
            game.drawShowDataButton();
            game.drawGatherDataButton();
        }
    }
    
    //====== private methods =======
    this.getPlayersClubStats = function() {
        return JSON.parse(GM_getValue(game.PLAYERS_CLUB_STATS, "[]"));
    }
    
    this.setPlayersClubStats = function(data) {
        GM_setValue(game.PLAYERS_CLUB_STATS, JSON.stringify(data));
    }
        
    this.gatherData = function() {
        var playersClubStats = game.getPlayersClubStats();
        
        var players = [];
        $('.profile_club_list_players .table_small tbody tr').each(function(){
            var nick = $(this).children('td:nth-child(2)').text().trim();
            var lvl = $(this).children('td:nth-child(3)').text().trim().replace(' ', '');
            var mk = $(this).children('td:nth-child(4)').text().trim().replace(' ', '');
            var turnament = $(this).children('td:nth-child(5)').text().trim().replace(' ', '');
            var turnamentWon = $(this).children('td:nth-child(6)').text().trim().replace(' ', '');
            var prestige = $(this).children('td:nth-child(7)').text().trim().replace(' ', '');
            
            
            var player = {
                nick:nick,
                lvl:lvl,
                mk:mk,
                turnament:turnament,
                turnamentWon:turnamentWon,
                prestige:prestige
            };
            
            players.push(player);
        });
        
        var date = new Date();
        
        var stats = {
            date:date,
            players:players
        };
        
        playersClubStats.push(stats);
        
        game.setPlayersClubStats(playersClubStats);
        console.log('Dane zostały zapisane');
    }
    
    this.drawGatherDataButton = function() {
        var gatherDataButton = createElement('input', {type:'button', value:'Pobierz dane', id:'gatherData'});
        
        $('.profile_club_list_players_header').append(gatherDataButton);
        
        $('#gatherData').click(function() {
            game.gatherData();
        });
    }
    
    this.drawShowDataButton = function() {
        var showDataButton = createElement('input', {type:'button', value:'Pokaż dane', id:'showData'});
        
        $('.profile_club_list_players_header').append(showDataButton);
        
        $('#showData').click(function() {
            game.drawShowDataBody();
        });
    }
    
    this.drawShowDataBody = function() {
        var showDataBody = createElement('div', {id:'showDataBody', style:'position: fixed; left: 5px; top: 5px; width: 200px; height: 200px; background: white; z-index: 10000;'}, document.body);
        var showDataBodyCloseButton = createElement('input', {type:'button', value:'Zamknij', id:'showDataBodyCloseButton', style:'position: absolute; right: -65px; top: 0px; z-index: 10000;'}, showDataBody);

        $('#showDataBodyCloseButton').click(function() {
            showDataBody.remove();
        });
        
        var playersClubStats = game.getPlayersClubStats();
        
        var firstSelect = createElement('select', {id:'firstSelect'}, showDataBody);
        var secondSelect = createElement('select', {id:'secondSelect'}, showDataBody);
        var thirdSelect = createElement('select', {id:'thirdSelect', style:'position: absolute; left: 0px; bottom: 0px;'}, showDataBody);
        
        for(var i in playersClubStats) {
            createElement('option', {value:playersClubStats[i].date}, firstSelect, playersClubStats[i].date);
            createElement('option', {value:playersClubStats[i].date}, secondSelect, playersClubStats[i].date);
            createElement('option', {value:playersClubStats[i].date}, thirdSelect, playersClubStats[i].date);
        }

        var compareButton = createElement('input', {type:'button', value:'Porównaj', id:'compareButton'}, showDataBody);
        
        $('#compareButton').click(function() {
            var firstDate = $('#firstSelect').val();
            var secondDate = $('#secondSelect').val();
            
            game.drawCompareDataBody(firstDate, secondDate);
        });
        
        
        var compareButtonForMessage = createElement('input', {type:'button', value:'Porównaj (do wysłania)', id:'compareButtonForMessage'}, showDataBody);
        
        $('#compareButtonForMessage').click(function() {
            var firstDate = $('#firstSelect').val();
            var secondDate = $('#secondSelect').val();
            
            game.drawCompareDataBodyForMessage(firstDate, secondDate);
        });
        
        var deleteButton = createElement('input', {type:'button', value:'Usuń wybrane statystyki', id:'deleteButton', style:'position: absolute; left: 0px; bottom: 20px; z-index: 10000;'}, showDataBody);
        
        $('#deleteButton').click(function() {
            var newPlayersClubStats = [];
            var date = $('#thirdSelect').val();
                
            for(var i in playersClubStats) {
                if(date != playersClubStats[i].date) {
                    newPlayersClubStats.push(playersClubStats[i]);
                }
            }
            
            game.setPlayersClubStats(newPlayersClubStats);
            showDataBody.remove();
        });
    }
    
    this.drawCompareDataBody = function(firstDate, secondDate) {
        var playersClubStats = game.getPlayersClubStats();
        var firstStats = null;
        var secondStats = null;
        
        for(var i in playersClubStats) {
            if(firstDate == playersClubStats[i].date) {
                firstStats = playersClubStats[i].players;
            }
            
            if(secondDate == playersClubStats[i].date) {
                secondStats = playersClubStats[i].players;
            }
        }
        
        if(firstStats != null && secondStats != null) {
            var compareDataBody = createElement('div', {id:'compareDataBody', style:'position: fixed; right: 5px; top: 20px; width: 800px; height: 400px; background: white; overflow-y: scroll; z-index: 10000;'}, document.body);
            var compareDataBodyCloseButton = createElement('input', {type:'button', value:'Zamknij', id:'compareDataBodyCloseButton', style:'position: fixed; right: 0px; top: 0px; z-index: 10000;'}, compareDataBody);
        
            $('#compareDataBodyCloseButton').click(function() {
                compareDataBody.remove();
            });
            
            var compareDataBodyTable = createElement('table', {id:'compareDataBodyTable', style:'border: 1px solid black; z-index: 10000;'}, compareDataBody);
            var compareDataBodyTableHeaderTr = createElement('tr', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTable);
            createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableHeaderTr, "Nick");
            createElement('td', {style:'border: 1px solid black; z-index: 10000;', colspan:3}, compareDataBodyTableHeaderTr, "Poziom");
            createElement('td', {style:'border: 1px solid black; z-index: 10000;', colspan:3}, compareDataBodyTableHeaderTr, "MK");
            createElement('td', {style:'border: 1px solid black; z-index: 10000;', colspan:3}, compareDataBodyTableHeaderTr, "Turnieje");
            createElement('td', {style:'border: 1px solid black; z-index: 10000;', colspan:3}, compareDataBodyTableHeaderTr, "Turnieje wygrane");
            createElement('td', {style:'border: 1px solid black; z-index: 10000;', colspan:3}, compareDataBodyTableHeaderTr, "Prestiż");
            
            for(var i in firstStats) {
                var secondStatsPlayer = game.findPlayer(secondStats, firstStats[i].nick);
                
                var compareDataBodyTableTr = createElement('tr', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTable);
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, firstStats[i].nick);
                
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, firstStats[i].lvl);
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, secondStatsPlayer.lvl);
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, (Number(secondStatsPlayer.lvl) - Number(firstStats[i].lvl)).toString());
                
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, firstStats[i].mk);
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, secondStatsPlayer.mk);
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, (Number(secondStatsPlayer.mk) - Number(firstStats[i].mk)).toString());
                
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, firstStats[i].turnament);
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, secondStatsPlayer.turnament);
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, (Number(secondStatsPlayer.turnament) - Number(firstStats[i].turnament)).toString());
                
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, firstStats[i].turnamentWon);
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, secondStatsPlayer.turnamentWon);
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, (Number(secondStatsPlayer.turnamentWon) - Number(firstStats[i].turnamentWon)).toString());
                
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, firstStats[i].prestige);
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, secondStatsPlayer.prestige);
                createElement('td', {style:'border: 1px solid black; z-index: 10000;'}, compareDataBodyTableTr, (Number(secondStatsPlayer.prestige) - Number(firstStats[i].prestige)).toString());
            }
        }
    }
    
    this.drawCompareDataBodyForMessage = function(firstDate, secondDate) {
        var playersClubStats = game.getPlayersClubStats();
        var firstStats = null;
        var secondStats = null;
        
        for(var i in playersClubStats) {
            if(firstDate == playersClubStats[i].date) {
                firstStats = playersClubStats[i].players;
            }
            
            if(secondDate == playersClubStats[i].date) {
                secondStats = playersClubStats[i].players;
            }
        }
        
        if(firstStats != null && secondStats != null) {
            var compareDataBody = createElement('div', {id:'compareDataBody', style:'position: fixed; right: 5px; top: 20px; width: 800px; height: 400px; background: white; overflow-y: scroll; z-index: 10000;'}, document.body);
            
            var spaces = 10;
            
            var text = "";            
            text += "MK";
            text += getSpaces(spaces-"MK".length-1);
            text += "|";
            text += "Turnieje";
            text += getSpaces(spaces-"Turnieje".length+2);
            text += "|";
            text += "Prestiż";
            text += getSpaces(spaces-"Prestiż".length+2);
            text += "|";
            text += "Nick";
            text += "<br>";
            
            for(var i in firstStats) {
                var secondStatsPlayer = game.findPlayer(secondStats, firstStats[i].nick);
                

                if(secondStatsPlayer == null) {
                    secondStatsPlayer = firstStats[i];
                    
                    secondStatsPlayer.mk = 0;
                    secondStatsPlayer.turnament = 0;
                    secondStatsPlayer.prestige = 0;
                }
                
                var mk = (Number(secondStatsPlayer.mk) - Number(firstStats[i].mk)).toString();
                var turnament = (Number(secondStatsPlayer.turnament) - Number(firstStats[i].turnament)).toString();
                var prestige = (Number(secondStatsPlayer.prestige) - Number(firstStats[i].prestige)).toString();
                
                text += mk;
                text += getSpaces(spaces-mk.length);
                text += "|";
                text += turnament;
                text += getSpaces(spaces-turnament.length);
                text += "|";
                text += prestige;
                text += getSpaces(spaces-prestige.length);
                text += "|";
                text += firstStats[i].nick;
                text += "<br>";
            }
            
            $('#compareDataBody').html(text);
        }
        
        var compareDataBodyCloseButton = createElement('input', {type:'button', value:'Zamknij', id:'compareDataBodyCloseButton', style:'position: fixed; right: 0px; top: 0px; z-index: 10000;'}, compareDataBody);

        $('#compareDataBodyCloseButton').click(function() {
            compareDataBody.remove();
        });
    }
    
    this.findPlayer = function(secondStats, playerNick) {
        for(var i in secondStats) {
            if(secondStats[i].nick == playerNick) {
                return secondStats[i];
            }
        }
        
        return null;
    }
}
 

$(document).ready(function() {
    game.render();
});